import React from "react";
import AceEditor from "react-ace";
import Grid from "@material-ui/core/Grid";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Chip from "@material-ui/core/Chip";

import "./App.css";
import "./index.css";

import "brace/theme/github"; // this is needed as the default theme
import "brace/mode/jsx";
import "brace/ext/searchbox";

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: "20px"
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular
  }
});

function ResultPanel(props) {
  const { classes } = props;

  const Label = () => {
    if (props.mle) {
      return (
        <Chip
          label="Memory Limit Exceeds"
          style={{ backgroundColor: "#ff5555", color: "#f8f8f2" }}
          className="chip"
        />
      );
    } else if (props.timeout) {
      return (
        <Chip
          label="Time Limit Exceeds"
          style={{ backgroundColor: "#ff5555", color: "#f8f8f2" }}
          className="chip"
        />
      );
    } else if (!props.pass) {
      return (
        <Chip
          label="Fail"
          style={{ backgroundColor: "#ff5555", color: "#f8f8f2" }}
          className="chip"
        />
      );
    } else {
      return <Chip label="Success" color="primary" className="chip" />;
    }
  };

  return (
    <div className={classes.root}>
      <ExpansionPanel defaultExpanded={!props.pass}>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography variant="h5" component="h3">
            #{props.number + 1}
            <Label/>
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <div className="papera">
            <div>
              <Typography>Contributor : {props.contributor}</Typography>
            </div>
            <br />

            <div>
              <Typography>Time : {props.duration}ms</Typography>
            </div>
            <br />

            <div>
              <h3 style={{color:"#ff5555"}}>
                  <pre>{props.error}</pre>
              </h3>
           </div>
            <Typography>Problem:</Typography>
            <div>
              <Grid container spacing={24}>
                <Grid item xs={12}>
                  <div className="border">
                    <AceEditor
                      theme="github"
                      editorProps={{ $blockScrolling: true }}
                      value={props.problem}
                      height="250px"
                      width="100%"
                      mode="text"
                      readOnly={true}
                      fontSize={17}
                      highlightActiveLine={false}
                    />
                    ,
                  </div>
                </Grid>
                <Grid item xs={6}>
                  <Typography>Expected:</Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography>Your Answer:</Typography>
                </Grid>
                <Grid item xs={6}>
                  <div className="border">
                    <AceEditor
                      theme="github"
                      editorProps={{ $blockScrolling: true }}
                      value={props.expected}
                      height="250px"
                      width="100%"
                      mode="text"
                      readOnly={true}
                      fontSize={17}
                      highlightActiveLine={false}
                    />
                    ,
                  </div>
                </Grid>
                <Grid item xs={6}>
                  <div className="border">
                    <AceEditor
                      theme="github"
                      editorProps={{ $blockScrolling: true }}
                      value={props.answer}
                      height="250px"
                      width="100%"
                      mode="text"
                      readOnly={true}
                      fontSize={17}
                      highlightActiveLine={false}
                    />
                    ,
                  </div>
                </Grid>
              </Grid>
            </div>
            <br></br>
          </div>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </div>
  );
}

ResultPanel.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ResultPanel);
