import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

class TestCase {
    private static ArrayList<String[]> cachedTestCases = null;
    private static String[] cachedTestDetail = null;

    private static String sendGet(String url) throws Exception {


        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");
        if(cachedTestDetail!=null && cachedTestCases!=null) {
            con.setConnectTimeout(5000);
        }

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();

    }

    static String[][] getTestCase(String testName) {
        System.out.println(String.format("Menerima test case %s ...",testName));
        ArrayList<String[]> testCases = new ArrayList<>();
        try {
            String res = sendGet("http://aren-api.herokuapp.com/grade-case?testName=" + testName);
            JSONObject resJ = new JSONObject(res);
            JSONArray cases = resJ.getJSONArray("cases");
            for (int i = 0; i < cases.length(); i++) {
                JSONObject theCase = cases.getJSONObject(i);
                String[] theCaseString = {
                        theCase.getString("problem"),
                        theCase.getString("answer"),
                        theCase.getString("contributor"),

                };
                testCases.add(theCaseString);
            }
        } catch (Exception e) {
            if (cachedTestCases != null) {
                System.out.println("Gagal menyambungkan ke server, menggunakan cached test case");
                return cachedTestCases.toArray(new String[cachedTestCases.size()][]);
            }
            e.printStackTrace();
        }
        if(cachedTestCases!=null) {
            cachedTestCases = testCases;
        }
        return testCases.toArray(new String[testCases.size()][]);
    }


    static String[] getTestDetails(String testName) {
        System.out.println(String.format("Menerima detail test %s ...",testName));
        String res;
        String[] details = new String[3];
        try {
            res = sendGet("http://aren-api.herokuapp.com/test-detail?testName=" + testName);
            JSONObject resJ = new JSONObject(res);
            details[0] = resJ.getString("time_out");
            details[1] = resJ.getString("class_name");
            details[2] = resJ.getString("memory_limit");
        } catch (Exception e) {
            if(cachedTestDetail!=null){
                System.out.println("Gagal menyambungkan ke server, menggunakan cached test detail");
                return cachedTestDetail;
            }
            e.printStackTrace();
        }
        if(cachedTestDetail!=null) {
            cachedTestDetail = details;
        }
        return details;
    }
}
