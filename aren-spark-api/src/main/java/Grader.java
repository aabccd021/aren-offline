import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

class Grader {

    //in
    private String sourceCode;

    //
    private String[][] testCases;
    private String className;
    private int timeOutRestriction;
    private int memoryLimit;

    //out
    boolean compileSuccess;
    String compileErrorMessage = "";
    private ArrayList<JSONObject> caseResults = new ArrayList<>();

    Grader(String sourceCode, String testName) throws IOException {

        System.out.println("Melakukan inisialisasi grader...");
        String[] test_detail = TestCase.getTestDetails(testName);

        this.sourceCode = sourceCode;
        this.testCases = TestCase.getTestCase(testName);
        this.timeOutRestriction = Integer.parseInt(test_detail[0]);
        this.className = test_detail[1];
        this.memoryLimit = Integer.parseInt(test_detail[2]);
        System.out.println(String.format("Time Limit: %s ms",this.timeOutRestriction));
        System.out.println(String.format("Memory Limit: %s Mb",this.memoryLimit));

        createSourceCodeFile(this.sourceCode, this.className);
        createTestCaseFile(this.testCases, this.className);
    }


    JSONObject gradingResult() throws JSONException {
        JSONObject result = new JSONObject();

        result.put("isCompileError", !this.compileSuccess);
        result.put("compileErrorMessage", this.compileErrorMessage);
        result.put("caseResults", this.caseResults);

        return result;
    }

    private static void createSourceCodeFile(String sourceCode, String className) throws IOException {
        System.out.println(String.format("Membuat file source code %s.java ...", className));
        FileOutputStream fos = new FileOutputStream(String.format("%s.java", className));
        fos.write(sourceCode.getBytes());
        fos.flush();
        fos.close();
    }

    private static void createTestCaseFile(String[][] testCases, String className) throws IOException {
        System.out.println(String.format("Membuat file test case %stc1.in - %stc%s.in ...", className, className, testCases.length));
        int i = 1;
        for (String[] tc : testCases) {
            FileOutputStream fos = new FileOutputStream(String.format("%stc%s.in", className, i));
            String input = tc[0];
            fos.write(input.getBytes());
            fos.flush();
            fos.close();
            i++;
        }
    }

    void doCompileTest() {
        System.out.println("Melakukan compile test...");
        try {

            if (!this.sourceCode.contains(className)) {
                this.compileSuccess = false;
                this.compileErrorMessage += "\nTolong gunakan nama class yang sesuai yaitu " + this.className;
                throw new Exception();
            }
            if (this.sourceCode.contains("System.exit")) {
                this.compileSuccess = false;
                this.compileErrorMessage += "\nJangan gunakan method 'System.exit()' karena aren nya juga akan berhenti";
                throw new Exception();
            }


            //===Compile=== === === ===
            ProcessBuilder processBuilder = new ProcessBuilder("javac", String.format("%s.java", this.className));
            processBuilder.directory(new File(System.getProperty("user.dir")));
            Process process = processBuilder.start();
            String error = IOUtils.toString(process.getErrorStream());
            if (!process.waitFor(1000, TimeUnit.MILLISECONDS)) {
                System.out.println("Destroy");
                process.destroy();
            }
            //=== === === === === ===


            if (!error.isEmpty()) {
                this.compileErrorMessage += String.format("\n %s", error);
                throw new Exception();
            }
            this.compileSuccess = true;

        } catch (Exception e) {
            System.out.println("Failed to compile source code: ");
            e.printStackTrace();
            this.compileSuccess = false;
            this.compileErrorMessage += "\n" + e.toString();
        }
    }

    void doGrading() throws IOException, InterruptedException {
        System.out.println("Melakukan Grading...");

        //result
        ArrayList<JSONObject> caseResults = new ArrayList<>();


        int caseNumber = 1;
        for (String[] testCase : this.testCases) {
            System.out.print(String.format("grading case no.%03d...", caseNumber));
            JSONObject caseResult = caseGradeResult(testCase, caseNumber);
            caseResults.add(caseResult);
            caseNumber++;
            System.out.println();
        }
        this.caseResults = caseResults;
        System.out.println("\nDone grading");
    }

    private JSONObject caseGradeResult(String[] testCase, int caseNumber) throws IOException, InterruptedException {

        //declare needed
        long startTime;
        long endTime;
        long duration;
        boolean mle = false;
        boolean timeout = false;
        boolean pass = false;
        String answer;

        //in
        String problem = testCase[0].replace("\\n", "\n");
        String expected = testCase[1].replace("\\n", "\n");
        String contributor = testCase[2];

        ProcessBuilder processBuilder =
                new ProcessBuilder("java", String.format("-Xmx%sm", this.memoryLimit), String.format("%s", this.className))
                        .redirectInput(new File(String.format("%stc%s.in", this.className, caseNumber)));
        processBuilder.directory(new File(System.getProperty("user.dir")));

        startTime = System.nanoTime();

        Process process = processBuilder.start();
        String errorContent = IOUtils.toString(process.getErrorStream());
        String outContent = IOUtils.toString(process.getInputStream());
        if (!process.waitFor(this.timeOutRestriction, TimeUnit.MILLISECONDS)) {
            System.out.println("Destroy");
            process.destroyForcibly();
        }
//        process.waitFor(this.timeOutRestriction, TimeUnit.MILLISECONDS);
//        process.destroyForcibly();
        endTime = System.nanoTime();


        duration = (endTime - startTime);

        System.out.print(String.format(" # Time: %sms", duration / 1000000));

        answer = outContent.trim().replace(String.valueOf((char) 0x0D), "");
        expected = expected.trim().replace(String.valueOf((char) 0x0D), "");

        if (expected.equals(answer)) {
            pass = true;
        }

        if (duration > (this.timeOutRestriction * 1000000)) {
            timeout = true;
        }

        if (answer.contains("OutOfMemoryError")) {
            mle = true;
        }

        if (timeout || mle) {
            pass = false;
        }


        if (pass) {
            System.out.print(" # Result: Pass");
        }else if(mle) {
            System.out.print(" # Result: Memory Limit Exceeds");
        }else if(timeout){
            System.out.print(" # Result: Time Limit Exceeds");
        } else {
            System.out.print(" # Result: Failed");
        }

        JSONObject result = new JSONObject();
        try {
            result.put("problem", problem);
            result.put("expected", expected);
            result.put("answer", answer);
            result.put("error", errorContent);
            result.put("timeout", timeout);
            result.put("mle", mle);
            result.put("pass", pass);
            result.put("duration", duration);
            result.put("contributor", contributor);
        } catch (Exception e) {
            System.out.println("ERROR : " + e.toString());
        }
        return result;
    }

    void cleanFiles() {
        File directory = new File(System.getProperty("user.dir"));
        File[] files = directory.listFiles();
        assert files != null;
        for (File f : files) {
            if (f.getName().contains(this.className)) {
                f.delete();
            }
        }
    }
}
