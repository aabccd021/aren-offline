import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static spark.Spark.*;

public class App {
    public static void main(String[] args) {
        System.out.println();
        System.out.println(aren21String);
        System.out.println("Memulai server...");

        System.out.println("Melakukan konfigurasi server...");
        port(2121);
        BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.INFO);
        staticFiles.location("/build");

        post("/grader/", "*/*", (request, response) -> {

            System.out.println("Menerima request grading...");
            System.out.println("Fetching request body...");
            JSONObject body = new JSONObject(request.body());
            String sourceCode = body.getString("code");
            String name = body.getString("name");


            Grader grader = new Grader(sourceCode, name);

            grader.doCompileTest();
            if (grader.compileSuccess) {
                System.out.println("Berhasil Compile");
                grader.doGrading();
            } else {
                System.out.println("Gagal Compile");
                System.out.println(grader.compileErrorMessage);
            }
            JSONObject result = grader.gradingResult();
            grader.cleanFiles();
            return result.toString();
        });

        System.out.println("Mapping API Functions...");
        get("/hello", (req, res) -> "Hello World");
        get("/", (req, res) -> renderContent());
        options("/*",
                (request, response) -> {

                    String accessControlRequestHeaders = request
                            .headers("Access-Control-Request-Headers");
                    if (accessControlRequestHeaders != null) {
                        response.header("Access-Control-Allow-Headers",
                                accessControlRequestHeaders);
                    }

                    String accessControlRequestMethod = request
                            .headers("Access-Control-Request-Method");
                    if (accessControlRequestMethod != null) {
                        response.header("Access-Control-Allow-Methods",
                                accessControlRequestMethod);
                    }

                    return "OK";
                });
        before((request, response) -> response.header("Access-Control-Allow-Origin", "*"));

        openWebpage();
    }

    private static void openWebpage() {
        System.out.println("Membuka Laman...");
        try {
            Desktop.getDesktop().browse(new URL("http://localhost:2121").toURI());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static String renderContent() {
        try {
            URL url = App.class.getResource("build/index.html");

            Path path = Paths.get(url.toURI());
            return new String(Files.readAllBytes(path), Charset.defaultCharset());
        } catch (IOException | URISyntaxException ignored) {
        }
        return null;
    }


    private static final String aren21String =
            "  /$$$$$$                             /$$$$$$   /$$  \n" +
                    " /$$__  $$                           /$$__  $$/$$$$  \n" +
                    "| $$  \\ $$ /$$$$$$  /$$$$$$ /$$$$$$$|__/  \\ $|_  $$  \n" +
                    "| $$$$$$$$/$$__  $$/$$__  $| $$__  $$ /$$$$$$/ | $$  \n" +
                    "| $$__  $| $$  \\__| $$$$$$$| $$  \\ $$/$$____/  | $$  \n" +
                    "| $$  | $| $$     | $$_____| $$  | $| $$       | $$  \n" +
                    "| $$  | $| $$     |  $$$$$$| $$  | $| $$$$$$$$/$$$$$$\n" +
                    "|__/  |__|__/      \\_______|__/  |__|________|______/";

}
